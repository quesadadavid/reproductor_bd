package com.example.david.ldm_bdmm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.database.DBAdapter;
import com.example.david.database.ReproductorIO;
import com.example.david.listview.*;

import java.util.ArrayList;
import java.util.Objects;

import static java.lang.System.exit;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{

    private DBAdapter db_adapter;
    private Button play;
    private Button repetir;
    private TextView artista, titulo, cronometro, tiempo_total;
    private int posicion;
    private boolean repeticion;
    private ArrayList<Cancion> lista_canciones;
    private ArrayList<Integer> id_inicializadas;
    private int contador_cargadas;
    private SeekBar seekbar;
    private ImageView caratula;

    private final Handler handler = new Handler();

    private final Runnable updatePositionRunnable = new Runnable() {
        public void run() {
            updatePosition();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        pedirPermisoEscribir();
    }

    private void metodoOnCreate() {
        ImageView imagen = findViewById(R.id.imageView4);
        posicion = 0;
        repeticion = false;

        db_adapter = new DBAdapter(this);
        db_adapter.open();

        if (db_adapter.tablaIsEmpty()) {
            lista_canciones = ReproductorIO.obtenerCancionesMemoria(this, db_adapter.getTablaCanciones());
            if (lista_canciones.isEmpty()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                builder.setTitle(R.string.no_musica);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exit(0);
                    }
                });

                Dialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                imagen.setVisibility(View.INVISIBLE);
                return;
            }
        } else {
            lista_canciones = ReproductorIO.obtenerTagCancionesBD(db_adapter.getTablaCanciones());
            if (ReproductorIO.actualizarDB(ReproductorIO.cancionesMemoria(this), ReproductorIO.cancionesDB(lista_canciones),
                    db_adapter.getTablaCanciones(), lista_canciones, this)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                builder.setTitle(R.string.actualizado);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                Dialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }

        contador_cargadas = 0;
        id_inicializadas = new ArrayList<>();
        for (int i = 0; i < 10; i++)
            id_inicializadas.add(-1);

        seekbar = findViewById(R.id.seekBar);

        inicializarCancion(lista_canciones.get(posicion));

        play = findViewById(R.id.btn_play);
        repetir = findViewById(R.id.btn_repetir);
        artista = findViewById(R.id.artista_txt);
        titulo = findViewById(R.id.titulo_txt);
        cronometro = findViewById(R.id.cronometro);
        tiempo_total = findViewById(R.id.tiempo_total);
        caratula = findViewById(R.id.caratula);
        ListView lv_canciones = findViewById(R.id.lista_canciones);

        cronometro.setText(timeFormat(0));
        seekbar.setMax(lista_canciones.get(posicion).getDuracion());

        imagen.setVisibility(View.INVISIBLE);
        actualizarLayout(lista_canciones.get(posicion));

        ArrayList categorities = ArrayListACategory(lista_canciones);
        AdapterCategory adapterCategory = new AdapterCategory(this, categorities);
        lv_canciones.setAdapter(adapterCategory);

        lv_canciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != posicion) {
                    Cancion cancion = lista_canciones.get(posicion);

                    if (cancion.noInicializada())
                        inicializarCancion(cancion);

                    if (cancion.getMedia_p().isPlaying()) {
                        cancion.getMedia_p().pause();
                        handler.removeCallbacks(updatePositionRunnable);
                    }

                    posicion = position;
                    cancion = lista_canciones.get(posicion);

                    if (cancion.noInicializada())
                        inicializarCancion(cancion);

                    cancion.getMedia_p().seekTo(0);
                    cancion.getMedia_p().start();
                    updatePosition();
                    actualizarLayout(cancion);
                } else {
                    Cancion cancion = lista_canciones.get(posicion);
                    if (!cancion.getMedia_p().isPlaying()) {
                        cancion.getMedia_p().seekTo(0);
                        cancion.getMedia_p().start();
                        updatePosition();
                        actualizarLayout(cancion);
                    }
                }
            }
        });

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            boolean userTouch;

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                userTouch = false;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                userTouch = true;
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                MediaPlayer mPlayer = lista_canciones.get(posicion).getMedia_p();
                if (mPlayer.isPlaying() && fromUser)
                    mPlayer.seekTo(progress);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainactivity_actionbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                auxAyuda();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Inicializa la cancion que se pasa como parámetro
     * @param c cancion
     */
    private void inicializarCancion (Cancion c){
        for(Integer i : id_inicializadas){
            if(i == c.getId_db())
                return;
        }

        if (id_inicializadas.get(contador_cargadas) != -1){
            int id_cancion = id_inicializadas.get(contador_cargadas);
            for(Cancion cancion : lista_canciones) {
                if (cancion.getId_db() == id_cancion){
                    cancion.setArray_B(null);
                    cancion.setMedia_p(null);
                }
            }
        }

        id_inicializadas.set(contador_cargadas, (int) c.getId_db());
        contador_cargadas = (contador_cargadas + 1) % 10;
        c.inicializarCancion(db_adapter.getTablaCanciones(), this);
        c.getMedia_p().setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (repeticion){
                    mp.seekTo(0);
                    mp.start();
                    updatePosition();
                } else {
                    if (posicion < lista_canciones.size() - 1) {
                        posicion++;
                        Cancion cancion = lista_canciones.get(posicion);
                        if (cancion.noInicializada())
                            inicializarCancion(cancion);
                        cancion.getMedia_p().seekTo(0);
                        cancion.getMedia_p().start();
                        updatePosition();

                    } else {
                        posicion = 0;
                        Cancion cancion = lista_canciones.get(posicion);
                        if (cancion.noInicializada())
                            inicializarCancion(cancion);
                        cancion.getMedia_p().seekTo(0);
                        cancion.getMedia_p().start();
                        updatePosition();
                    }
                    actualizarLayout(lista_canciones.get(posicion));
                }
            }
        });

        seekbar.setMax(c.getMedia_p().getDuration());
    }

    protected void onDestroy() {
        db_adapter.close();
        super.onDestroy();
    }

    /**
     * OnClick de btn_play
     *
     * @param v view
     */
    public void play(View v) {
        Cancion cancion = lista_canciones.get(posicion);

        if (cancion.noInicializada())
            inicializarCancion(cancion);

        if (cancion.getMedia_p().isPlaying()) {
            cancion.getMedia_p().pause();
            handler.removeCallbacks(updatePositionRunnable);
            play.setBackgroundResource(R.drawable.play);
        } else {
            cancion.getMedia_p().start();
            play.setBackgroundResource(R.drawable.pause);
            updatePosition();
            actualizarLayout(cancion);
        }
    }

    /**
     * OnClick de btn_anterior
     *
     * @param v view
     */
    public void anterior(View v) {
        Cancion cancion = lista_canciones.get(posicion);

        if (cancion.noInicializada())
            inicializarCancion(cancion);

        if (posicion >= 1) {
            if (cancion.getMedia_p().isPlaying()) {
                cancion.getMedia_p().pause();
                handler.removeCallbacks(updatePositionRunnable);

                posicion--;
                cancion = lista_canciones.get(posicion);
                if (cancion.noInicializada())
                    inicializarCancion(cancion);

                cancion.getMedia_p().seekTo(0);
                cancion.getMedia_p().start();
                updatePosition();
            } else {
                posicion--;
                cancion = lista_canciones.get(posicion);

                if (cancion.noInicializada())
                    inicializarCancion(cancion);

                cancion.getMedia_p().seekTo(0);
                cancion.getMedia_p().start();
                updatePosition();
            }
        }
        actualizarLayout(cancion);
    }

    /**
     * OnClick de btn_siguiente
     *
     * @param v view
     */
    public void siguiente(View v) {
        Cancion cancion = lista_canciones.get(posicion);
        if (cancion.noInicializada())
            inicializarCancion(cancion);

        if (posicion < lista_canciones.size() - 1) {
            if (cancion.getMedia_p().isPlaying()) {
                cancion.getMedia_p().pause();
                handler.removeCallbacks(updatePositionRunnable);
                posicion++;
                cancion = lista_canciones.get(posicion);

                if (cancion.noInicializada())
                    inicializarCancion(cancion);
                cancion.getMedia_p().seekTo(0);
                cancion.getMedia_p().start();
                updatePosition();
            } else {
                posicion++;
                cancion = lista_canciones.get(posicion);

                if (cancion.noInicializada())
                    inicializarCancion(cancion);
                cancion.getMedia_p().seekTo(0);
                cancion.getMedia_p().start();
                updatePosition();
            }
        } else if (posicion == lista_canciones.size() - 1) {
            if (cancion.getMedia_p().isPlaying()) {
                cancion.getMedia_p().pause();
                handler.removeCallbacks(updatePositionRunnable);
                posicion = 0;
                cancion = lista_canciones.get(posicion);

                if (cancion.noInicializada())
                    inicializarCancion(cancion);
                cancion.getMedia_p().seekTo(0);
                cancion.getMedia_p().start();
                updatePosition();
            } else {
                posicion = 0;
                cancion = lista_canciones.get(posicion);

                if (cancion.noInicializada())
                    inicializarCancion(cancion);
                cancion.getMedia_p().seekTo(0);
                cancion.getMedia_p().start();
                updatePosition();
            }
        }
        actualizarLayout(cancion);
    }

    /**
     * OnClick de btn_repetir
     *
     * @param v view
     */
    public void repertir(View v) {
        if (repeticion) {
            repetir.setBackgroundResource(R.drawable.reproducir_blanco);
            lista_canciones.get(posicion).getMedia_p().setLooping(false);
            repeticion = false;
        } else {
            repetir.setBackgroundResource(R.drawable.reproducir_verde);
            lista_canciones.get(posicion).getMedia_p().setLooping(true);
            repeticion = true;
        }
    }

    private void updatePosition(){
        handler.removeCallbacks(updatePositionRunnable);

        seekbar.setProgress(lista_canciones.get(posicion).getMedia_p().getCurrentPosition());
        cronometro.setText(timeFormat(lista_canciones.get(posicion).getMedia_p().getCurrentPosition()));

        handler.postDelayed(updatePositionRunnable, 0);
    }

    private void actualizarLayout(Cancion cancion) {
        artista.setText(cancion.getArtista());
        tiempo_total.setText(timeFormat(cancion.getDuracion()));
        titulo.setText(cancion.getTitulo());
        if(cancion.getCaratula() != null)
            caratula.setImageBitmap(cancion.getCaratula());
        else
            caratula.setImageResource(R.drawable.caratula_musica);

        if (cancion.getMedia_p().isPlaying())
            play.setBackgroundResource(R.drawable.pause);
        else
            play.setBackgroundResource(R.drawable.play);

    }

    private void auxAyuda(){
        Intent intent =  new Intent(MainActivity.this, ayuda.class);
        this.startActivity(intent);
    }

    /**
     * Pide permisos para acceder al almacenamiento del dispositivo para leer
     */
    private void pedirPermisoLeer() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        else
            metodoOnCreate();
    }

    /**
     * Pide permisos para acceder al almaceamiento del dispositivo para escribir
     */
    private void pedirPermisoEscribir() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        else
            metodoOnCreate();
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
        builder.setTitle(R.string.salir);
        builder.setPositiveButton(R.string.btn_salir, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                lista_canciones.get(posicion).getMedia_p().stop();
                exit(0);
            }
        });

        builder.setNegativeButton(R.string.quedarse, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {

        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED)
                        pedirPermisoEscribir();
                    else
                        metodoOnCreate();
                } else {
                    Toast.makeText(this, R.string.no_creada, Toast.LENGTH_SHORT).show();
                    pedirPermisoLeer();
                }
                return;
            }
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED)
                        pedirPermisoLeer();
                    else
                        metodoOnCreate();
                } else {
                    Toast.makeText(this, R.string.no_creada, Toast.LENGTH_SHORT).show();
                    pedirPermisoEscribir();
                }
            }
        }
    }

    /**
     * Metodo privado que genera un ArrayList de Category con el que inicializar el ListView
     *
     * @param lista lista de canciones
     * @return lista_category
     */
    private ArrayList ArrayListACategory(ArrayList<Cancion> lista) {
        ArrayList<Category> lista_category = new ArrayList<>();
        for (Cancion cancion : lista) {
            String artista = cancion.getArtista();
            String titulo = cancion.getTitulo();
            String duracion = timeFormat(cancion.getDuracion());
            Bitmap caratula = cancion.getCaratula();
            lista_category.add(new Category(titulo, artista, duracion, caratula));
        }
        return lista_category;
    }

    /**
     * Metodo privado que devuelve el tiempo como String en formato 00:00
     *
     * @param time tiempo
     * @return string de tiempo
     */
    private String timeFormat(int time) {
        time = time / 1000; //en formato segundos
        int minutos = time / 60;
        int segundos = time % 60;
        if (segundos >=10)
            return (minutos + ":" + segundos);
        else
            return (minutos + ":0" + segundos);
    }

}
