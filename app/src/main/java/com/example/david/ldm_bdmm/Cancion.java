package com.example.david.ldm_bdmm;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;

import com.example.david.database.TablaCanciones;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Cancion {
    private long id_db;
    private long id_fichero;
    private MediaPlayer media_p;
    private String titulo;
    private String artista;
    private int duracion;
    private byte[] array_B;
    private Bitmap caratula;


    /**
     *
     * @param id_db x
     * @param id_fichero x
     * @param path x
     * @param titulo x
     * @param artista x
     * @param duracion x
     * @param context x
     * @param caratula x
     */
    public Cancion(long id_db, long id_fichero, String path, String titulo, String artista, int duracion, Context context, Bitmap caratula) {
        this.id_fichero = id_fichero;
        this.id_db = id_db;
        this.titulo = titulo;
        this.artista = artista;
        this.duracion = duracion;
        try {
            this.array_B = blob(pathToFile(path));

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.media_p = byteArrayToMediaPlayer(this.array_B, context);
        this.caratula = caratula;
    }

    /**
     *
     * @param id_db x
     * @param id_fichero x
     * @param titulo x
     * @param artista x
     * @param duracion x
     * @param caratula x
     */
    public Cancion(long id_db, long id_fichero, String titulo, String artista, int duracion, Bitmap caratula) {
        this.id_fichero = id_fichero;
        this.id_db = id_db;
        this.titulo = titulo;
        this.artista = artista;
        this.duracion = duracion;
        this.media_p = null;
        this.array_B = null;
        this.caratula = caratula;
    }

    /**
     * Método que convierte un arrya de Byte en un objeto de la clase mediaPlayer
     *
     * @param cancion   array de bytes
     * @param context   contexto
     * @return media player del array list
     */
    private static MediaPlayer byteArrayToMediaPlayer(byte[] cancion, Context context) {
        MediaPlayer mp = new MediaPlayer();

        try {
            File tempMP3 = File.createTempFile("cancion", "mp3", context.getCacheDir());
            tempMP3.deleteOnExit();

            FileOutputStream fos = new FileOutputStream(tempMP3);
            fos.write(cancion);
            fos.close();

            FileInputStream fis = new FileInputStream(tempMP3);
            mp.setDataSource(fis.getFD());
            mp.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mp;
    }

    /**
     * Devuelve el FileInputStream generado a partir de un path de entrada
     *
     * @param path  path a un file
     * @return  file desde el path de entrada
     */
    private static FileInputStream pathToFile(String path) {
        File file = new File(path);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fis;
    }

    /**
     * Devuelve un array de Bytes a partir de un FileInputStream de entrada
     *
     * @param fis   FileInputStream
     * @return  array de bytes desde el file del fis
     * @throws IOException si no se encuentra el file
     */
    private static byte[] blob(FileInputStream fis) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int read;
        while ((read = fis.read(buffer)) != -1) {
            baos.write(buffer, 0, read);
        }
        baos.flush();

        return baos.toByteArray();
    }

    /**
     * Inicializa la variable privada media_p de tipo mediaPlayer
     *
     * @param tabla tabla
     * @param context contexto
     */
    void inicializarCancion(TablaCanciones tabla, Context context) {

        byte[] blob;
        int longitud = tabla.longitudBlob((int) this.id_db);
        if (longitud >= 1000000)
            blob = tabla.getBlobGrande(longitud, (int) this.id_db);
        else
            blob = tabla.getBlobPeq((int) this.id_db);

        this.media_p = byteArrayToMediaPlayer(blob, context);
    }

    boolean noInicializada() {
        return media_p == null;
    }

    /*GETTERS Y SETTERS*/
    public long getId_db(){return this.id_db;}

    public long getId_fichero(){
        return this.id_fichero;
    }

    String getTitulo() {
        return titulo;
    }

    String getArtista() {
        return artista;
    }

    int getDuracion() {
        return duracion;
    }

    MediaPlayer getMedia_p() {
        return media_p;
    }

    public byte[] getArray_B() {
        return array_B;
    }

    public void setArray_B(byte[] bytes){
        this.array_B = bytes;
    }

    public void setMedia_p(MediaPlayer media_p){
        this.media_p = media_p;
    }

    public Bitmap getCaratula() {
        return caratula;
    }

    public void setCaratula(Bitmap caratula) {
        this.caratula = caratula;
    }
}