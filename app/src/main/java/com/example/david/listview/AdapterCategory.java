package com.example.david.listview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.david.ldm_bdmm.R;

import java.util.ArrayList;

public class AdapterCategory extends ArrayAdapter {
    private Context context;
    private ArrayList<Category> datos;

    public AdapterCategory (Context context, ArrayList datos) {
        super(context,R.layout.item_category,datos);
        this.context = context;
        this.datos = datos;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint({"ViewHolder", "InflateParams"}) View item = inflater.inflate(R.layout.item_category, null);

        TextView titulo = item.findViewById(R.id.titulo_lv);
        titulo.setText(datos.get(position).getTitulo());

        TextView artista =item.findViewById(R.id.artista_lv);
        artista.setText(datos.get(position).getArtista());

        TextView duracion =item.findViewById(R.id.duracion_lv);
        duracion.setText(datos.get(position).getDuracion());

        ImageView caratula = item.findViewById(R.id.caratula);
        if(datos.get(position).getCaratula() != null)
            caratula.setImageBitmap(datos.get(position).getCaratula());
        else
            caratula.setImageResource(R.drawable.caratula_musica);

        return item;
    }
}
