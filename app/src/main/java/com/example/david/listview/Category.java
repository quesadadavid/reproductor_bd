package com.example.david.listview;

import android.graphics.Bitmap;

public class Category {

    private String titulo;
    private String artista;
    private String duracion;
    private Bitmap caratula;

    public Category(String titulo, String artista, String duracion, Bitmap caratula) {
        super();
        this.titulo = titulo;
        this.artista = artista;
        this.duracion = duracion;
        this.caratula = caratula;
    }

    /* GETTERS*/
    String getTitulo() {
        return titulo;
    }

    String getArtista() {
        return artista;
    }

    String getDuracion() {
        return duracion;
    }

    Bitmap getCaratula(){
        return caratula;
    }
}