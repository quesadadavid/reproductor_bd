package com.example.david.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Clase de apoyo para la gestion de la base de datos
 */
public class DBAdapter {
    private final static int VERSION = 1;
    private final static String DB_NAME = "Ejemplo.db";

    private DBOpenHelper dbHelper;
    private SQLiteDatabase sqlDB;
    private TablaCanciones tablaCanciones;

    /**
     * Constructor
     *
     * @param context conttexto
     */
    public DBAdapter(Context context) {
        dbHelper = new DBOpenHelper(context);
    }

    public void open() {
        sqlDB = dbHelper.getWritableDatabase();
        dbHelper.onCreate(sqlDB);
        tablaCanciones = new TablaCanciones(sqlDB);
    }

    public void close() {
        sqlDB.close();
    }

    public TablaCanciones getTablaCanciones() {
        return this.dbHelper.getTablaCanciones();
    }

    /**
     * Devuelve true si la tablaCanciones esta vacía
     *
     * @return true o false
     */
    public boolean tablaIsEmpty() {
        return tablaCanciones.isEmpty();
    }

    private class DBOpenHelper extends SQLiteOpenHelper {

        DBOpenHelper(Context context) {
            super(context, DB_NAME, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TablaCanciones.CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

        TablaCanciones getTablaCanciones() {
            return tablaCanciones;
        }
    }


}








