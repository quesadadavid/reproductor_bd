package com.example.david.database;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import com.example.david.ldm_bdmm.Cancion;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Clase auxiliar con metodos para la gestion de la base de datos y el contenido multimedia
 */
public class ReproductorIO {

    /**
     * Devuelve un ArrayList de objetos de la clase Cancion y guarda datos en la base de datos
     * de la app
     *
     * @param context         contexto
     * @param tabla_canciones tablaCanciones
     * @return Array de canciones
     */
    public static ArrayList<Cancion> obtenerCancionesMemoria(Context context, TablaCanciones tabla_canciones) {
        ArrayList<Cancion> lista_canciones = new ArrayList<>();
        long id_db = 1;
        ContentResolver musicResolver = context.getContentResolver();
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        @SuppressLint("Recycle") Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);

        if (musicCursor != null && musicCursor.moveToFirst()) {
            //Obtener columnas
            int titleColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ARTIST);
            int duracionColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            int pathColumn = musicCursor.getColumnIndex(MediaStore.MediaColumns.DATA);

            //añadir canciones a la lista y guardarlas en la bd
            do {
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                String thisPaht = musicCursor.getString(pathColumn);
                int thisDuracion = musicCursor.getInt(duracionColumn);
                //byteArray y Bitmap
                Bitmap thisImage = getBitmap(thisPaht);
                byte[] imagenByteArray = null;
                if(thisImage != null)
                    imagenByteArray = bitmapToByteArray(thisImage);

                Cancion c = new Cancion(id_db, thisId, thisPaht, thisTitle, thisArtist, thisDuracion, context, thisImage);
                tabla_canciones.insert((int) thisId, thisTitle, thisArtist, c.getArray_B(), thisDuracion, imagenByteArray);
                c.setArray_B(null);
                c.setMedia_p(null);
                lista_canciones.add(c);
                id_db++;
            } while (musicCursor.moveToNext() && musicCursor.getPosition() < 50);
        }
        return lista_canciones;
    }

    /**
     * Devuelve un ArrayList de objetos de la clase Cancion a partir de la base de datos de la app
     *
     * @param tabla   tablaCacniones
     * @return Array de canciones
     */
    public static ArrayList<Cancion> obtenerTagCancionesBD(TablaCanciones tabla) {
        ArrayList<Cancion> lista_canciones = new ArrayList<>();
        Cursor musicCursor = tabla.getCursorSinCancion();

        if (musicCursor != null && musicCursor.moveToFirst()) {
            //Obtener columnas
            int idDBColumn = musicCursor.getColumnIndex("id_db");
            int idFileColumn = musicCursor.getColumnIndex("id_file");
            int titleColumn = musicCursor.getColumnIndex("titulo");
            int artistColumn = musicCursor.getColumnIndex("artista");
            int duracionColumn = musicCursor.getColumnIndex("duracion");
            int caratulaColum = musicCursor.getColumnIndex("caratula");

            //añadir canciones a la lista
            do {
                long thisIdDB = musicCursor.getLong(idDBColumn);
                long thisIdFile = musicCursor.getLong(idFileColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                int thisDuracion = musicCursor.getInt(duracionColumn);
                byte[] thisCaratula = musicCursor.getBlob(caratulaColum);
                Bitmap bitmapCaratula = null;
                if(thisCaratula != null)
                    bitmapCaratula = byteArrayToBitmap(thisCaratula);

                lista_canciones.add(new Cancion(thisIdDB, thisIdFile, thisTitle, thisArtist, thisDuracion, bitmapCaratula));
            } while (musicCursor.moveToNext());
        }
        return lista_canciones;
    }

    /**
     *
     * @param context contexto
     * @return arrayList de id_fichero almacenados en la base de datos
     */
    public static ArrayList<Long> cancionesMemoria(Context context){
        ContentResolver musicResolver = context.getContentResolver();
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        @SuppressLint("Recycle") Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
        ArrayList<Long> id_ficheros_memoria = new ArrayList<>();

        if (musicCursor != null && musicCursor.moveToFirst()) {

            int idColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media._ID);

            do {
                long thisId = musicCursor.getLong(idColumn);
                id_ficheros_memoria.add(thisId);

            } while (musicCursor.moveToNext() && musicCursor.getPosition() < 50);
        }
        return id_ficheros_memoria;
    }

    /**
     *
     * @param canciones arraylist de canciones
     * @return arrayList de id_fichero almacenados en la lista de canciones actual
     */
    public static ArrayList<Long> cancionesDB(ArrayList<Cancion> canciones){
        ArrayList<Long> id_ficheros_db = new ArrayList<>();
        for(Cancion c : canciones){
            id_ficheros_db.add(c.getId_fichero());
        }
        return id_ficheros_db;
    }

    /**
     * Actualiza la base de datos cuando se introducen canciones nuevas
     * @param c_memoira x
     * @param c_DB x
     * @param tabla x
     * @param lista_canciones x
     * @param context x
     * @return x
     */
    public static boolean actualizarDB(ArrayList<Long> c_memoira, ArrayList<Long> c_DB, TablaCanciones tabla,
                                       ArrayList<Cancion> lista_canciones, Context context){
        if(!c_memoira.equals(c_DB)){
            for(Long l : c_DB){
                if(!c_memoira.contains(l)) {
                    tabla.delete(l.intValue());
                    for(Cancion c : lista_canciones){
                        if(c.getId_fichero() == l) {
                            lista_canciones.remove(c);
                            break;
                        }
                    }
                }
            }

            for(Long l : c_memoira){
                if(!c_DB.contains(l)){
                    ContentResolver musicResolver = context.getContentResolver();
                    Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    @SuppressLint("Recycle") Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);

                    if (musicCursor != null && musicCursor.moveToFirst()) {
                        //Obtener columnas
                        int titleColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
                        int idColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media._ID);
                        int artistColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ARTIST);
                        int duracionColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
                        int pathColumn = musicCursor.getColumnIndex(MediaStore.MediaColumns.DATA);

                        //añadir canciones a la lista y guardarlas en la bd
                        do {
                            long thisId = musicCursor.getLong(idColumn);
                            if(l.equals(thisId)) {
                                String thisTitle = musicCursor.getString(titleColumn);
                                String thisArtist = musicCursor.getString(artistColumn);
                                String thisPaht = musicCursor.getString(pathColumn);
                                int thisDuracion = musicCursor.getInt(duracionColumn);
                                Bitmap thisImage = getBitmap(thisPaht);
                                byte[] imagenByteArray = null;
                                if (thisImage != null)
                                    imagenByteArray = bitmapToByteArray(thisImage);

                                long id_db = lista_canciones.size();
                                int j = 0;
                                for(Cancion c : lista_canciones){
                                    if(c.getId_db() != j)
                                        id_db = j;
                                }
                                Cancion c = new Cancion(id_db, thisId, thisPaht, thisTitle, thisArtist, thisDuracion, context, thisImage);
                                tabla.insert((int) thisId, thisTitle, thisArtist, c.getArray_B(), thisDuracion, imagenByteArray);
                                c.setArray_B(null);
                                lista_canciones.add(c);
                            }
                        } while (musicCursor.moveToNext() && musicCursor.getPosition() < 50);
                    }
                }
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     *
     * @param pathID id de un mp3
     * @return bitmap de la caratula del disco
     */
    private static Bitmap getBitmap(String pathID){
        MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
        metaRetriver.setDataSource(pathID);
        Bitmap songImage;
        try{
            byte[] art = metaRetriver.getEmbeddedPicture();
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inSampleSize = 2;
            songImage = BitmapFactory.decodeByteArray(art, 0, art.length,opt);
        } catch (Exception e){
            songImage = null;
        }
        return songImage;
    }

    /**
     *
     * @param bitmap bitmap
     * @return bytearray
     */
    private static byte[] bitmapToByteArray(Bitmap bitmap){
        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /* Ignored for PNGs */, blob);
        return blob.toByteArray();
    }

    /**
     *
     * @param byteArray byteArray
     * @return bitmap
     */
    private static Bitmap byteArrayToBitmap(byte[] byteArray){
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    /**
     * @param a array de bytes 1
     * @param b array de bytes 2
     * @return array de bytes 1 y 2 concatenados
     */
    static byte[] concatenar(byte[] a, byte[] b){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write( a );
            outputStream.write( b );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray( );
    }
}

