package com.example.david.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class TablaCanciones {
    // Nombre de la tabla
    private final static String NAME = "tabla_canciones";

    // Objeto que utilizamos para acceder a la base de datos.
    private SQLiteDatabase sqlDB;

    /**
     * Constructor
     *
     * @param sqlDB SQLiteDatabase
     */
    TablaCanciones(SQLiteDatabase sqlDB) {
        this.sqlDB = sqlDB;
    }

    /**
     * Clase que contiene los campos de la tabla.
     */
    public class Columns implements BaseColumns {
        final static String ID_DB = "id_db";
        final static String ID_FILE = "id_file";
        final static String TITULO = "titulo";
        final static String ARTISTA = "artista";
        final static String CANCION = "cancion";
        final static String DURACION = "duracion";
        final static String CARATULA = "caratula";
    }

    // Sentencia para crear la tabla en la BD.
    final static String CREATE_TABLE = "create table if not exists "
            + NAME + "(" + Columns.ID_DB + " integer primary key autoincrement, " + Columns.ID_FILE + " integer not null, "
            + Columns.TITULO + " varchar(30) not null, " + Columns.ARTISTA
            + " varchar(30) not null, " + Columns.CANCION + " blob, " + Columns.DURACION + " integer, "
            + Columns.CARATULA + " blob) ";

    /**
     * Inserta una nueva tupla en la tabla
     */
    void insert(int id_file, String titulo, String artista, byte[] cancion, int duracion, byte[] caratula) {
        // Relacionamos campos con valores en el objeto ContentValues.
        ContentValues values = new ContentValues();
        values.put(Columns.ID_FILE, id_file);
        values.put(Columns.TITULO, titulo);
        values.put(Columns.ARTISTA, artista);
        values.put(Columns.CANCION, cancion);
        values.put(Columns.DURACION, duracion);
        values.put(Columns.CARATULA, caratula);

        sqlDB.insert(NAME, null, values);
    }

    /**
     * borra una cancion de la base de datos
     * @param id_fichero id del fichero que se va a borrar
     */
    void delete(int id_fichero){
        sqlDB.delete(NAME, Columns.ID_FILE + "=" + id_fichero, null);
    }

    /**
     * Devuelve el cursor con todos los nombres registrados en la tabla.
     *
     * @return Cursor
     */
    Cursor getCursorSinCancion() {
        return sqlDB.rawQuery("SELECT id_db, id_file, titulo, artista, duracion, caratula FROM tabla_canciones",null);
    }

    /**
     * Devuelve el nombre de la tabla.
     *
     * @return NAME
     */
    public String getName() {
        return NAME;
    }


    /**
     * Devuelve true si la tabla esta vacía
     * @return true si esta vacia
     */
    @SuppressLint("Recycle")
    boolean isEmpty(){
        String count = "SELECT count(*) FROM " + NAME;
        Cursor mcursor = sqlDB.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        return icount <= 0;
    }

    private SQLiteDatabase getSQLite(){
        return this.sqlDB;
    }

    public int longitudBlob(int id){
        String[] args =  new String[]{Integer.toString(id)};
        Cursor c = sqlDB.rawQuery("SELECT length(cancion) FROM tabla_canciones WHERE id_db = ?",args);
        c.moveToFirst();
        int longitud = c.getInt(0);
        c.close();
        return longitud;
    }

    public byte[] getBlobGrande(int longitud, int id) {
        int lim1 = longitud/500000;
        int lim2 = longitud%500000;
        byte[] aux;
        byte[] cancion = new byte[0];
        Cursor c;
        String[] args =  new String[]{Integer.toString(id)};

        for (int i = 0; i<lim1; i++){ //sacamos el blob de 1MB en 1MB

            c = this.getSQLite().rawQuery("SELECT substr(cancion, " + (i*500000 + 1) + ", 500000)" +
                    " FROM tabla_canciones WHERE id_db = ?", args);

            c.moveToFirst();
            aux = c.getBlob(0);
            cancion = ReproductorIO.concatenar(cancion,aux);
        }

        c = this.getSQLite().rawQuery("SELECT substr(cancion, " + (longitud-lim2+1) + ", " +
                lim2 + ") FROM tabla_canciones WHERE id_db = ?", args);

        c.moveToFirst();
        aux = c.getBlob(0);
        cancion = ReproductorIO.concatenar(cancion, aux);

        c.close();

        return cancion;
    }

    public byte[] getBlobPeq(int id) {
        String[] args =  new String[]{Integer.toString(id)};
        @SuppressLint("Recycle") Cursor c = this.getSQLite().rawQuery("select cancion from tabla_canciones where id_db = ?",args);
        c.moveToFirst();
        return c.getBlob(0);
    }
}
